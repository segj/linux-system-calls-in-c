/* Computer architecture and operating systems project
 * Source file 1
 * Sasa Jalander, 2102321
*/

#include <sys/wait.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


// remove in place everything that is not a lower case letter
void remove_garbage(char *str) {

	// printf("string before removing garbage: %s", str);

	for (int i = 0; i < strlen(str); i++) {

		if ((str[i] < 'a') || (str[i] > 'z')) {
			memmove(&str[i], &str[i+1], strlen(str) - i);
			i--;
		}
	}

	return;
}


int main() {

	printf("P0 PID: %d\n", getpid());

	// get input from user
	printf("Please input the test string\n> ");
	char input_string[100];
	fgets(input_string, 100, stdin);
	printf("\n");
	// printf("%s", input_string);
	

	// create pipe for communicating to child process
	int pipe_array[2];

	if (pipe(pipe_array) == -1) {
		printf("pipe failed");
		exit(1);
	}
	
	// create child process
	pid_t fork_pid = fork();

	if (fork_pid < 0) {
		printf("fork failed");


	// C0
	} else if (fork_pid == 0) {

		printf("C0 PID: %d\n", getpid());

		// close writing-end of pipe
		close(pipe_array[1]);

		// read from pipe and close it
		char child_string[100];
		read(pipe_array[0], child_string, 100);
		close(pipe_array[0]);
		printf("C0 read the following string from pipe: %s", child_string);

		// clean the string
		remove_garbage(child_string);

		// create variables for shared memory creation
		key_t mem_key = 873234;
		int mem_size = 1024;
		int shmid;
		char *mem_str_ptr;

		// create shared memory
		shmid = shmget(mem_key, mem_size, IPC_CREAT | 0666);

		if (shmid < 0) {
			perror("C0 shmget failed");
		}

		// attach shared memory
		mem_str_ptr = shmat(shmid, NULL, 0);

		if (mem_str_ptr == (char *) -1) {
			perror("C0 shmat failed");
		}

		// write in shared memory
		strncpy(mem_str_ptr, child_string, mem_size);

		// create child process to exec
		pid_t fork_pid2 = fork();

		if (fork_pid2 < 0) {
			printf("fork2 failed");

		// P1
		} else if (fork_pid2 == 0) {

			// turn child into a new process
			char* arguments[] = {"s2", NULL};
			execv("s2", arguments);

		// C0
		} else {

			// wait for P1
			wait(NULL);

			// delete shared memory segment
			shmctl(shmid, IPC_RMID, NULL);

			printf("Shared memory segment deleted (check with ipcs -m)\n");
			printf("C0 terminated\n");
		}


	// P0
	} else {

		// close reading-end of pipe
		close(pipe_array[0]);

		// write to pipe and close it
		write(pipe_array[1], input_string, strlen(input_string) + 1);
		close(pipe_array[1]);

		// wait for C0
		wait(NULL);
		printf("P0 terminated\n");
	}
}

