/* Computer architecture and operating systems project
 * Source file 2
 * Sasa Jalander, 2102321
*/

#include <sys/wait.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, const char *argv[]) {

	printf("\nP1 PID: %d\n", getpid());

	key_t mem_key = 873234;
	int mem_size = 1024;
	int shmid;
	char *mem_str_ptr;
	char input_string[mem_size];

	// access shared memory
	shmid = shmget(mem_key, mem_size, 0666);

	if (shmid < 0) {
		perror("P1 shmget failed");
	}

	mem_str_ptr = shmat(shmid, NULL, 0);

	if (mem_str_ptr == (char *) -1) {
		perror("P1 shmat failed");
	}

	// read shared memory
	strncpy(input_string, mem_str_ptr, mem_size);
	printf("P1 read the following string from shared memory: %s\n", input_string);

	// detach from shared memory
	shmdt(mem_str_ptr);

	// create list of missing characters
	char char_list[27] = "abcdefghijklmnopqrstuvwxyz";
	int found;

	for (int i = 0; i < strlen(char_list); i++) {

		found = 0;

		for (int j = 0; j < strlen(input_string); j++) {
			if (char_list[i] == input_string[j]) {
				found = 1;
			}
		}

		if (found) {
			memmove(&char_list[i], &char_list[i+1], strlen(char_list) - i);
			i--;
		}
	}

	printf("MISSING ALPHABET CHARACTERS: %s\n", char_list);

	printf("\nP1 terminated\n");
}

